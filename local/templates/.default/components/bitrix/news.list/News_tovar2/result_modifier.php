<?
var_dump($arResult);
$arTempID = array();
foreach($arResult["ITEMS"] as $elem)
{
    $arTempID[] = $elem["PROPERTIES"]["TOVAR"]["VALUE"];
}

$arSort = false;
$arFilter = array(
    "IBLOCK_ID" => IBLOCK_TOVAR,
    "ACTIVE" => "Y",
    "ID" => $arTempID,
);
$arGroupBy = false;
$arNavStarParams = array("ntopCount" => 50);
$arSelect = array("ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_PRICE", "PROPERTY_SIZE");
$BDRes = CIBlockElement::GetList(
    $arSort,
    $arFilter,
    $arGroupBy,
    $arNavStarParams,
    $arSelect
);
$arResult["CAT_ELEM"] = array();
while($arRes = $BDRes->GetNext())
{
    $arResult["CAT_ELEM"][$arRes["ID"]] = $arRes;
}
?>