<?
function test_dump($var, $die = false, $all = false)
{
	global $USER;
	if (($USER->GetID() == 1) || ($all == true)) {
?>
		<font style="text-align: left; font-size: 12px">
			<pre><? var_dump($var) ?></pre>
		</font><br>
	<?
	}
	if ($die) {
		die;
	}
}

function my_dump($var)
{
	global $USER;
	if ($USER->isAdmin()) {?>
	<pre>
		<? print_r($var) ?>
	</pre>
<?}
}
