<?php
define("IBLOCK_CAT_ID", 2);
define("IBLOCK_NEWS_ID", 1);

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/constants.php")) {
    require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/constants.php");
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/function.php")) {
    require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/function.php");
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/agent.php")) {
    require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/agent.php");
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/agent2.php")) {
    require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/agent2.php");
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/event_handlers.php")) {
    require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/event_handlers.php");
}