<?
//Проверка на символы
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", array("CIBLockHanlder", "OnBeforeIBlockElementUpdateHandler"));
class CIBLockHanlder
{
    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        if ($arFields['IBLOCK_ID'] == CATALOG_IBLOCK_ID) {
            $db_props = CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID'], array("sort" => "asc"), array("CODE" => "PRICE"));
            if ($ar_props = $db_props->Fetch()) {
                if (strlen($arFields['PROPERTY_VALUES'][$ar_props['ID']][$ar_props['PROPERTY_VALUE_ID']]['VALUE']) > 0) {
                    $arFields['PROPERTY_VALUES'][$ar_props['ID']][$ar_props['PROPERTY_VALUE_ID']]['VALUE'] = preg_replace("/[^\d]/", "", $arFields['PROPERTY_VALUES'][$ar_props['ID']][$ar_props['PROPERTY_VALUE_ID']]['VALUE']);
                }
            }
        }
    }
}

//Отмена удаления ИБ
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", array("CIBlockElementHandler", "OnBeforeIBlockElementUpdateHandler"));
class CIBlockElementHandler
{
    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        //Достаю дату создания
        CModule::IncludeModule("iblock");
        $arSelect = array("ID", "NAME", "DATE_CREATE");
        $arFilter = array("ID" => $arFields["ID"]);
        $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 50), $arSelect);
        $ob = $res->GetNextElement();
        $ob->GetFields();
        define("DATA_CREATE", $ob->fields["DATE_CREATE"]);
        //"17.06.2021 15:28:29"
        if ($arFields["ACTIVE"] == "N" && date_diff(new DateTime("24.06.2021 15:28:29"), new DateTime(DATA_CREATE))->days < 3) {
            global $APPLICATION;
            $APPLICATION->throwException("Вы деактивировали свежую новость");
            return false;
        }
    }
}


AddEventHandler("main", "OnBeforeEventAdd", array("CMainHandler", "OnBeforeEventAddHandler"));
AddEventHandler("main", "OnBeforeUserAdd", array("CMainHandler", "OnBeforeUserAddHandler"));
class CMainHandler
{
    function OnBeforeEventAddHandler(&$event, &$lid, &$arFields)
    {
        if ($event == "FEEDBACK_FORM") {
            if (CModule::IncludeModule("iblock")) {
                $el = new CIBlockElement;
                $arLoadProductArray = array(
                    "IBLOCK_ID" => FEEDBACK_IBLOCK_ID,
                    "NAME" => $arFields['AUTHOR'],
                    "DETAIL_TEXT" => $arFields['TEXT'],
                    "DATE_ACTIVE_FROM" => ConvertTimeStamp(false, "FULL"),
                );
                $el->Add($arLoadProductArray);
            }
        }
    }

    function OnBeforeUserAddHandler(&$arFields)
    {
        if ($arFields["LAST_NAME"] == $arFields["NAME"]) {
            global $APPLICATION;
            $APPLICATION->throwException("Имя и Фамилия одинаковы!");
            return false;
        }
    }
}




//Не удалять, если просмотры
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", array("CIBlockElementDelete", "OnBeforeIBlockElementDeleteHandler"));
class CIBlockElementDelete
{
    function OnBeforeIBlockElementDeleteHandler($ID)
    {
        //Достаю show_counter
            CModule::IncludeModule("iblock");
            $arSelect = array("ID", "NAME", "DATE_CREATE", "show_counter", "active");
            $arFilter = array("ID" => $ID);
            $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 50), $arSelect);
            $ob = $res->GetNextElement();
            $ob->GetFields();

        $countView = $ob->fields['SHOW_COUNTER'];
        if ($countView > 0) {
            global $APPLICATION;
            $APPLICATION->throwException("Элемент с ID=$ID нельзя удалить. Кол-во просмотров=$countView");
            return false;
        }
    }
}


//Оповещение о добавлении в группу
AddEventHandler("main", "OnBeforeUserUpdate", array("MyClass", "OnBeforeUserUpdateHandler"));
class MyClass
{
    function OnBeforeUserUpdateHandler(&$arFields)
    {
        $arGroups = CUser::GetUserGroup($arFields["ID"]);

        if (!in_array(5, $arGroups)) {
            test_dump($arGroups, 1);

            $filter = array("GROUPS_ID" => array(GROUP_ADMIN_ID));
            $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
            $arEmail = array();
            while ($arUser = $rsUsers->GetNext()) {
                $arEmail[] = $arUser['EMAIL'];
            }
            if (count($arEmail) > 0) {
                $arEventFields = array(
                    "TEXT" => 'Новый пользователь',
                    "EMAIL" => implode(", ", $arEmail),
                );
                CEvent::Send("CHECK_CATALOG", SITE_ID, $arEventFields);
            }
        }
    }
}
