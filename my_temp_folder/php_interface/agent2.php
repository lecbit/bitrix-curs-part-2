<?
function agentCheckPromotion()
{
    if (CModule::IncludeModule("iblock")) {
        $arSelect = array("ID", "NAME");
        $arFilter = array("IBLOCK_ID" => 12, "!ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
        $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 50), $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arItems[] = $arFields;
        }

        CEventLog::Add(array(
            "SEVERITY" => "SECURITY",
            "AUDIT_TYPE_ID" => "CHECK_PROMOTION",
            "MODULE_ID" => "iblock",
            "ITEM_ID" => "",
            "DESCRIPTION" => "Проверка валидности акций. Просрочено " . count($arItems) . "акция/ий",
        ));

        if (count($arItems) > 0) {
            $filter = array("GROUPS_ID" => array(1));
            $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
            $arEmail = array();
            while ($arUser = $rsUsers->GetNext()) {
                $arEmail[] = $arUser['EMAIL'];
            }
            if (count($arEmail) > 0) {
                $arEventFields = array(
                    "TEXT" => count($arItems),
                    "EMAIL" => implode(", ", $arEmail),
                );
                CEvent::Send("CHECK_CATALOG", SITE_ID, $arEventFields);
            }
        }
    }
    return "agentCheckPromotion();";
}
