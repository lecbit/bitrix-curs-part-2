<?
function agentCheckPrice()
{
    if (CModule::IncludeModule("iblock")) {
        $arSelect = array("ID", "NAME", "PROPERTY_PRICE");
        $arFilter = array("IBLOCK_ID" => CATALOG_IBLOCK_ID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "PROPERTY_PRICE" => false);
        $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 50), $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arItems[] = $arFields;
        }
        
        CEventLog::Add(array(
            "SEVERITY" => "SECURITY",
            "AUDIT_TYPE_ID" => "CHECK_PRICE",
            "MODULE_ID" => "iblock",
            "ITEM_ID" => "",
            "DESCRIPTION" => "Проверка цен, нет цен для ".count($arItems)." элементов",
        ));
        
        if (count($arItems) > 0) {
            $filter = array("GROUPS_ID" => array(GROUP_ADMIN_ID));
            $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
            $arEmail = array();
            while ($arUser = $rsUsers->GetNext()) {
                $arEmail[] = $arUser['EMAIL'];
            }
            if (count($arEmail) > 0) {
                $arEventFields = array(
                    "TEXT" => count($arItems),
                    "EMAIL" => implode(", ", $arEmail),
                );
                CEvent::Send("CHECK_CATALOG", SITE_ID, $arEventFields);
            }
        }
    }
    return "agentCheckPrice();";
}
