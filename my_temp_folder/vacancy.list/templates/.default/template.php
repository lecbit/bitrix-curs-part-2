<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? //test_dump($arResult, 1); ?>

<? if($arParams["IBLOCK_ID"] == 14): ?>

<div class="sb_nav" id="88">
<ul>
<?foreach ($arResult["SECTION_ID_MY"] as $key => $value):?>
<li class='close'><span class='sb_showchild'></span><a href><span> <?=$arResult["SECTION_ID_MY"][$key]["NAME"]?> </a></span>
<ul>

    <?foreach($arResult["ITEMS"] as $arItem => $value):?>
    <?
    if($value["IBLOCK_SECTION_ID"] == $key):
    //Кнопки эрмитажа//================================

    $this->AddEditAction($value['ID'], $value['EDIT_LINK'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT'));
    $this->AddDeleteAction($value['ID'], $value['DELETE_LINK'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')))?>
    <li id="<?=$this->GetEditAreaId($value['ID'])?>">
    <a class="ps_head_link" href="<?=$value["DETAIL_PAGE_URL"]?>"><?=$value["NAME"]?></a>
    </li>
    
    <?endif;?>
    <?endforeach;?>
</ul>
</li>
<?endforeach;?>
</ul>
</div>

<? endif; ?>