<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 180;

//Тут кэш, вооот
if ($this->StartResultCache(3600, $USER->GetGroups())) {
  if (!Loader::includeModule("iblock")) {
    $this->abortResultCache();
    ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
    return;
  }

  //Получаю раздел и элементы
  CModule::IncludeModule("iblock");
  $res = CIBlockSection::GetList(array("SORT" => "­­ASC"), array("IBLOCK_ID" => 14), false, $uf_name);
  while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arParams["SECTION_ID_MY"][$arFields["ID"]]["NAME"] = $arFields["NAME"];
  }

  $arSelect = array("ID", "NAME", "IBLOCK_SECTION_ID");
  $arFilter = array("IBLOCK_ID" => 14);
  $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 50), $arSelect);
  while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arParams['SECTION_ID_MY'][$arFields["IBLOCK_SECTION_ID"]]["NAME2"][] = $arFields["NAME"];
  }

  // Формирую эрмитаж
  $rsItems = CIBlockElement::GetList($arSort, $arFilter, false);
  while ($arItem = $rsItems->GetNext()) {
    $arButtons = CIBlock::GetPanelButtons(
      $arItem["IBLOCK_ID"],
      $arItem["ID"],
      0,
      array("SECTION_BUTTONS" => false, "SESSID" => false)
    );
    $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
    $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
    $arResult["ITEMS"][] = $arItem;
    $arResult["LAST_ITEM_IBLOCK_ID"] = $arItem["IBLOCK_ID"];
  }
  $this->setResultCacheKeys(array(
    $arResult["SECTION_ID_MY"] = $arParams["SECTION_ID_MY"],
    $arResult["ITEMS"],
    "LAST_ITEM_IBLOCK_ID",
  ));
  //Подключение шаблона
  $this->IncludeComponentTemplate();
}

if (
  $arResult["LAST_ITEM_IBLOCK_ID"] > 0
  && $USER->IsAuthorized()
  && $APPLICATION->GetShowIncludeAreas()
  && CModule::IncludeModule("iblock")
) {
  $arButtons = CIBlock::GetPanelButtons($arResult["LAST_ITEM_IBLOCK_ID"], 0, 0, array("SECTION_BUTTONS" => false));
  $this->addIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
}